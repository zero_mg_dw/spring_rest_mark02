package com.umg.mark01.api;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

//import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.context.annotation.ComponentScan;
//import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;
//import springfox.documentation.swagger2.annotations.EnableSwagger2;


@SpringBootApplication
/*@EnableAutoConfiguration
@EnableCaching*/
//@EnableSwagger2
/*@EntityScan(basePackages = {"com.umg.mark01.core.entities"})
@EnableJpaRepositories(basePackages = {"com.umg.mark01.api"})*/
@ComponentScan(basePackages = {"com.umg.mark01.api"})
//@EnableDiscoveryClient
//@EnableResourceServer
public class Mark01APIApplication {

    public static void main(String[] args) {

        SpringApplication.run(Mark01APIApplication.class, args);
    }


}
